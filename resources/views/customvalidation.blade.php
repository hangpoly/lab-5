<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Laravel : Custom Validation</title>
</head>
<body>
  <div>
    {{ Form::open(array('url'=>'custom-validation', 'method' => 'POST')) }}
      @if (count($errors)>0)
        <div style="color:red">
          <ul>
            @foreach ($errors->all() as $err)
            <li>{{$err}}</li>
            @endforeach
          </ul>
        </div>
      @endif
      {{ Form::text('phone', old('phone'), ['placeholder' => 'Enter Vietnam Phone number']) }}
      {{ Form::submit('Save')}}
    {{ Form::close() }}
  </div>
</body>
</html>