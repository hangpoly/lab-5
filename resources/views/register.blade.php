<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Register</title>
</head>
<body>
 <div style="margin: 80px auto;">
  {{Form::open(array('url'=>'register', 'method'=> 'POST'))}}
  @if (count($errors)>0)
    <div style="color:red">
      <ul>
        @foreach ($errors->all() as $err)
        <li>{{$err}}</li>
        @endforeach
      </ul>
    </div>
  @endif
  <table>
    <tr>
      <td colspan="2"><h3>Register</h3></td>
    </tr>
    <tr>
      <td>Username</td>
      <td>{{Form::text('username', old('username'))}}</td>
    </tr>
    <tr>
      <td>Email</td>
      <td>{{Form::email('email', old('email'))}}</td>
    </tr>
    <tr>
      <td>Password</td>
      <td>{{Form::password('password', old('password'))}}</td>
    </tr>
    <tr>
      <td>Mobile</td>
      <td>{{Form::text('phone', old('phone'), ['placeholder'=>'+8444...'])}}</td>
    </tr>
    <tr>
      <td>Date of birth</td>
      <td>{{Form::date('birth')}}</td>
    </tr>
    <tr>
      <td>Gender</td>
      <td>
        <select name="sex">
          <option value="male" selected>Male</option>
          <option value="female" selected>Female</option>
        </select>
      </td>
    </tr>
  </table>
  {{Form::submit('Register')}}
  {{Form::close()}}
 </div>
</body>
</html>