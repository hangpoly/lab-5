<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class ValidationController extends Controller
{
    public function showForm() {
        return view('login');
    }

    public function validationForm(Request $request){
        $this->validate($request, [
            'username'=> 'required|max:8',
            'password'=> 'required'
        ]);
    }
}
