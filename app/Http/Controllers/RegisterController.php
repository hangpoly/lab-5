<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index(){
        return view('register');
    }

    public function register(Request $request) {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'phone' => 'in_phone|min:12|max:13|required',
            'birth' => 'required|birth',
            'sex' => 'required'
        ]);
    }
}
