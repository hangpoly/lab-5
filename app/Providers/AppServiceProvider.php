<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('in_phone', function($attribute, $value){
            return substr($value, 0, 3) == '+84';
        });

        Validator::extend('birth', function($attribute, $value){
            return date($value) < date('Y-m-d');
        });
    }
}
