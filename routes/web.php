<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('validation', 'ValidationController@showForm');

Route::post('validation', 'ValidationController@validationForm');



Route::get('custom-validation', [
    'as'=> 'custom-validation.get',
    'uses'=> 'CustomValidatorController@getCustomValidation'
]);


Route::post('custom-validation', [
    'as'=> 'custom-validation.post',
    'uses'=> 'CustomValidatorController@postCustomValidation'
]);



Route::get('register', 'RegisterController@index');

Route::post('register', 'RegisterController@register');